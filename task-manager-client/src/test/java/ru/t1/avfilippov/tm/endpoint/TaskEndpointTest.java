package ru.t1.avfilippov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.avfilippov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.avfilippov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.avfilippov.tm.api.endpoint.IUserEndpoint;
import ru.t1.avfilippov.tm.api.service.IPropertyService;
import ru.t1.avfilippov.tm.dto.request.*;
import ru.t1.avfilippov.tm.dto.response.TaskListResponse;
import ru.t1.avfilippov.tm.enumerated.Sort;
import ru.t1.avfilippov.tm.marker.IntegrationCategory;
import ru.t1.avfilippov.tm.model.Task;
import ru.t1.avfilippov.tm.service.PropertyService;

import static ru.t1.avfilippov.tm.constant.TestClientData.*;

@Category(IntegrationCategory.class)
public final class TaskEndpointTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);


    @NotNull
    private static final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(propertyService);

    @NotNull
    private static final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @Nullable
    private static String token;

    @Nullable
    private static String tokenAdmin;

    @NotNull
    private final static String login = "ADMIN";

    @NotNull
    private final static String pass = "ADMIN";

    @Nullable
    private Task task;

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(login);
        loginRequest.setPassword(pass);
        tokenAdmin = authEndpoint.login(loginRequest).getToken();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(tokenAdmin);
        request.setLogin(USER2_TEST_LOGIN);
        request.setPassword(USER2_TEST_PASS);
        request.setEmail("test1@testing.com");
        userEndpoint.registryUser(request);
        @NotNull final UserLoginRequest loginUserRequest = new UserLoginRequest();
        loginUserRequest.setLogin(USER2_TEST_LOGIN);
        loginUserRequest.setPassword(USER2_TEST_PASS);
        token = authEndpoint.login(loginRequest).getToken();
    }

    private String getUserToken() throws Exception {
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(USER2_TEST_LOGIN);
        request.setPassword(USER2_TEST_PASS);
        return authEndpoint.login(request).getToken();
    }

    @Before
    public void before() throws Exception {
        @NotNull final String name = "Name";
        @NotNull final String description = "Description";
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(getUserToken());
        request.setName(name);
        request.setDescription(description);
        task = taskEndpoint.createTask(request).getTask();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @NotNull final TaskClearRequest request = new TaskClearRequest(token);
        taskEndpoint.clearTask(request);
        @NotNull final UserRemoveRequest requestUser = new UserRemoveRequest(tokenAdmin);
        requestUser.setLogin(USER2_TEST_LOGIN);
        userEndpoint.removeUser(requestUser);
    }

    @Test
    public void clearTask() throws Exception {
        @NotNull final TaskClearRequest request = new TaskClearRequest(getUserToken());
        Assert.assertNull(taskEndpoint.clearTask(request).getTask());
    }

    @Test
    public void createTask() throws Exception {
        @NotNull final String name = "Task";
        @NotNull final String description = "Description";
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(getUserToken());
        request.setName(name);
        request.setDescription(description);
        Assert.assertNotNull(taskEndpoint.createTask(request).getTask());
    }

    @Test
    public void getTaskById() throws Exception {
        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(getUserToken());
        request.setTaskId(task.getId());
        Assert.assertNotNull(taskEndpoint.showTaskById(request).getTask());
    }

    @Test
    public void getTaskByIndex() throws Exception {
        @NotNull final TaskShowByIndexRequest request = new TaskShowByIndexRequest(getUserToken());
        request.setIndex(0);
        Assert.assertNotNull(taskEndpoint.showTaskByIndex(request).getTask());
    }

    @Test
    public void listTask() throws Exception {
        @Nullable final String sort = null;
        @NotNull final TaskListRequest request = new TaskListRequest(getUserToken());
        request.setSort(sort);
        @NotNull final TaskListResponse response = taskEndpoint.listTasks(request);
        Assert.assertNotNull(response.getTasks());
    }

    @Test
    public void removeTaskById() throws Exception {
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(getUserToken());
        request.setTaskId(task.getId());
        Assert.assertNull(taskEndpoint.removeTaskById(request).getTask());
    }

    @Test
    public void startTaskById() throws Exception {
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(getUserToken());
        request.setTaskId(task.getId());
        Assert.assertNotNull(taskEndpoint.startTaskById(request).getTask());
    }

    @Test
    public void completeTaskById() throws Exception {
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(getUserToken());
        request.setTaskId(task.getId());
        Assert.assertNotNull(taskEndpoint.completeTaskById(request).getTask());
    }


    @Test
    public void updateTaskById() throws Exception {
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(getUserToken());
        request.setTaskId(task.getId());
        request.setName("Second Task");
        request.setDescription("Desc");
        Assert.assertNotNull(taskEndpoint.updateTaskById(request).getTask());
    }

}
