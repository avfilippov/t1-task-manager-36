package ru.t1.avfilippov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.dto.request.ProjectListRequest;
import ru.t1.avfilippov.tm.dto.response.ProjectListResponse;
import ru.t1.avfilippov.tm.enumerated.Sort;
import ru.t1.avfilippov.tm.model.Project;
import ru.t1.avfilippov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "display all projects";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECTS]");
        System.out.println("[ENTER SORT]");
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable final String sortType = TerminalUtil.nextLine();
        @NotNull final ProjectListRequest request = new ProjectListRequest(getToken());
        request.setSort(sortType);
        @Nullable final ProjectListResponse response = getProjectEndpoint().listProjects(request);
        if (response.getProjects() == null) response.setProjects(Collections.emptyList());
        @NotNull final List<Project> projects = response.getProjects();
        int index = 1;
        for (@NotNull final Project project : projects) {
            System.out.println(index + ". " + project.getName());
            index++;
        }
    }

}
