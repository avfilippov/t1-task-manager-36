package ru.t1.avfilippov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.dto.request.DataXmlLoadJaxBRequest;
import ru.t1.avfilippov.tm.enumerated.Role;

public class DataXmlLoadJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-xml-jaxb";

    @NotNull
    private static final String DESCRIPTION = "Load data from xml file";

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD XML]");
        @NotNull DataXmlLoadJaxBRequest request = new DataXmlLoadJaxBRequest();
        getDomainEndpoint().loadDataXmlJaxB(request);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
