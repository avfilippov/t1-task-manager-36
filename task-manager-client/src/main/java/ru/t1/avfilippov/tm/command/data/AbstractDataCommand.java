package ru.t1.avfilippov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.avfilippov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.avfilippov.tm.command.AbstractCommand;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    protected IDomainEndpoint getDomainEndpoint() {
        return serviceLocator.getDomainEndpoint();
    }


    public AbstractDataCommand() {
    }

}
