package ru.t1.avfilippov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.dto.request.TaskListRequest;
import ru.t1.avfilippov.tm.dto.response.TaskListResponse;
import ru.t1.avfilippov.tm.enumerated.Sort;
import ru.t1.avfilippov.tm.exception.entity.TaskNotFoundException;
import ru.t1.avfilippov.tm.model.Task;
import ru.t1.avfilippov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "display all tasks";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASKS]");
        System.out.println("[ENTER SORT]");
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable final String sortType = TerminalUtil.nextLine();
        @NotNull final TaskListRequest request = new TaskListRequest(getToken());
        request.setSort(sortType);
        @Nullable final TaskListResponse response = getTaskEndpoint().listTasks(request);
        if (response.getTasks() == null) response.setTasks(Collections.emptyList());
        renderTasks(response.getTasks());
    }

}
