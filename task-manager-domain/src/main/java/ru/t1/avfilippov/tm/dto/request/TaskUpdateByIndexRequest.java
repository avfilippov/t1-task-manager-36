package ru.t1.avfilippov.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TaskUpdateByIndexRequest extends AbstractUserRequest {

    @Nullable private Integer index;

    @Nullable private String name;

    @Nullable private String description;

    public TaskUpdateByIndexRequest(@Nullable final String token) {
        super(token);
    }

}
