package ru.t1.avfilippov.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TaskUpdateByIdRequest extends AbstractUserRequest {

    @Nullable private String taskId;

    @Nullable private String name;

    @Nullable private String description;

    public TaskUpdateByIdRequest(@Nullable final String token) {
        super(token);
    }

}
