package ru.t1.avfilippov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.avfilippov.tm.api.service.IProjectService;
import ru.t1.avfilippov.tm.api.service.IServiceLocator;
import ru.t1.avfilippov.tm.dto.request.*;
import ru.t1.avfilippov.tm.dto.response.*;
import ru.t1.avfilippov.tm.enumerated.Sort;
import ru.t1.avfilippov.tm.enumerated.Status;
import ru.t1.avfilippov.tm.model.Project;
import ru.t1.avfilippov.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.t1.avfilippov.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectChangeStatusByIdResponse changeProjectStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectChangeStatusByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String id = request.getProjectId();
        @Nullable final String statusValue = request.getStatusValue();
        @Nullable final Status status = Status.toStatus(statusValue);
        @Nullable final String userId = session.getUserId();
        @Nullable final Project project = getProjectService().changeProjectStatusById(userId, id, status);
        return new ProjectChangeStatusByIdResponse(project);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectChangeStatusByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String statusValue = request.getStatusValue();
        @Nullable final Status status = Status.toStatus(statusValue);
        @Nullable final String userId = session.getUserId();
        @Nullable final Project project = getProjectService().changeProjectStatusByIndex(userId, index, status);
        return new ProjectChangeStatusByIndexResponse(project);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectClearResponse clearProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectClearRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        getProjectService().clear(userId);
        return new ProjectClearResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectCompleteByIdResponse completeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCompleteByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String id = request.getProjectId();
        @Nullable final String userId = session.getUserId();
        @Nullable final Project project = getProjectService().changeProjectStatusById(userId, id, Status.COMPLETED);
        return new ProjectCompleteByIdResponse(project);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectCompleteByIndexResponse completeProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCompleteByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = session.getUserId();
        @Nullable final Project project = getProjectService().changeProjectStatusByIndex(userId, index, Status.COMPLETED);
        return new ProjectCompleteByIndexResponse(project);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectCreateResponse createProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCreateRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = session.getUserId();
        @Nullable final Project project = getProjectService().create(userId, name, description);
        return new ProjectCreateResponse(project);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectListResponse listProjects(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectListRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String sortType = request.getSort();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @Nullable final String userId = session.getUserId();
        @NotNull final List<Project> projects = getProjectService().findAll(userId, sort);
        return new ProjectListResponse(projects);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectRemoveByIdResponse removeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectRemoveByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String id = request.getProjectId();
        @Nullable final String userId = session.getUserId();
        @Nullable final Project project = getProjectService().findOneById(userId, id);
        if (project != null) getServiceLocator().getProjectTaskService().removeProjectById(userId, project.getId());
        return new ProjectRemoveByIdResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectRemoveByIndexResponse removeProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectRemoveByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = session.getUserId();
        @Nullable final Project project = getProjectService().findOneByIndex(userId, index);
        if (project != null) getServiceLocator().getProjectTaskService().removeProjectById(userId, project.getId());
        return new ProjectRemoveByIndexResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectShowByIdResponse showProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectShowByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String id = request.getProjectId();
        @Nullable final String userId = session.getUserId();
        @Nullable final Project project = getProjectService().findOneById(userId, id);
        return new ProjectShowByIdResponse(project);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectShowByIndexResponse showProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectShowByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = session.getUserId();
        @Nullable final Project project = getProjectService().findOneByIndex(userId, index);
        return new ProjectShowByIndexResponse(project);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectStartByIdResponse startProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectStartByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String id = request.getProjectId();
        @Nullable final String userId = session.getUserId();
        @Nullable final Project project = getProjectService().changeProjectStatusById(userId, id, Status.IN_PROGRESS);
        return new ProjectStartByIdResponse(project);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectStartByIndexResponse startProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectStartByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = session.getUserId();
        @Nullable final Project project = getProjectService().changeProjectStatusByIndex(userId, index, Status.IN_PROGRESS);
        return new ProjectStartByIndexResponse(project);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectUpdateByIdResponse updateProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectUpdateByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String id = request.getProjectId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = session.getUserId();
        @Nullable final Project project = getProjectService().updateById(userId, id, name, description);
        return new ProjectUpdateByIdResponse(project);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectUpdateByIndexResponse updateProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectUpdateByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = session.getUserId();
        @Nullable final Project project = getProjectService().updateByIndex(userId, index, name, description);
        return new ProjectUpdateByIndexResponse(project);
    }

}
