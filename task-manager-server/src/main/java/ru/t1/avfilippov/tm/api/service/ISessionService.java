package ru.t1.avfilippov.tm.api.service;

import ru.t1.avfilippov.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {

}
