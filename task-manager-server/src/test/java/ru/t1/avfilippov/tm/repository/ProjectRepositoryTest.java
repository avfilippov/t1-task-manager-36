package ru.t1.avfilippov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.avfilippov.tm.api.repository.IProjectRepository;
import ru.t1.avfilippov.tm.marker.DataCategory;
import ru.t1.avfilippov.tm.model.Project;

import static ru.t1.avfilippov.tm.constant.TestData.*;

@Category(DataCategory.class)
public final class ProjectRepositoryTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Before
    public void before() {
        projectRepository.add(USER_PROJECT1);
        projectRepository.add(ADMIN_PROJECT1);
    }

    @After
    public void after() {
        projectRepository.clear();
    }

    @Test
    public void add() {
        Assert.assertNotNull(projectRepository.add(USER_PROJECT2));
        @Nullable final Project project = projectRepository.findOneById(USER_PROJECT2.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT2, project);
        Assert.assertNull(projectRepository.add(NULL_PROJECT));
    }

    @Test
    public void addByUserId(){
        Assert.assertNotNull(projectRepository.add(USER1.getId(), USER_PROJECT2));
        Assert.assertNull(projectRepository.add(USER1.getId(),NULL_PROJECT));
        Assert.assertNull(projectRepository.add(null,USER_PROJECT2));
        @Nullable final Project project = projectRepository.findOneById(USER_PROJECT2.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT2, project);
    }

    @Test
    public void createdByUserId(){
        Assert.assertEquals(ADMIN_PROJECT1.getUserId(),USER2.getId());
    }

    @Test
    public void findAll(){
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_PROJECT1);
        Assert.assertEquals(USER_PROJECT1,emptyRepository.findOneById(USER_PROJECT1.getId()));
    }

    @Test
    public void findById() {
        Assert.assertNotNull(projectRepository.findOneById(USER1.getId(),USER_PROJECT1.getId()));
    }

    @Test
    public void removeById() {
        Assert.assertNotNull(projectRepository.removeById(ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeByIndex() {
        Assert.assertNotNull(projectRepository.removeByIndex(1));
    }

    @Test
    public void removeAll() {
        projectRepository.clear();
        Assert.assertTrue(projectRepository.findAll().isEmpty());
    }

    @Test
    public void create() {
        @NotNull final Project project = projectRepository.create(USER2.getId(), ADMIN_PROJECT1.getName(), ADMIN_PROJECT1.getDescription());
        Assert.assertEquals(ADMIN_PROJECT1.getDescription(), project.getDescription());
        Assert.assertEquals(ADMIN_PROJECT1.getName(), project.getName());
        Assert.assertEquals(USER2.getId(), project.getUserId());
    }

}
