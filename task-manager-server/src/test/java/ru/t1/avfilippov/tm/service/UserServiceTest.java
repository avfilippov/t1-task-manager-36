package ru.t1.avfilippov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.avfilippov.tm.api.repository.IProjectRepository;
import ru.t1.avfilippov.tm.api.repository.ITaskRepository;
import ru.t1.avfilippov.tm.api.repository.IUserRepository;
import ru.t1.avfilippov.tm.api.service.IPropertyService;
import ru.t1.avfilippov.tm.enumerated.Role;
import ru.t1.avfilippov.tm.exception.AbstractException;
import ru.t1.avfilippov.tm.exception.entity.UserNotFoundException;
import ru.t1.avfilippov.tm.exception.field.AbstractFieldException;
import ru.t1.avfilippov.tm.exception.field.LoginEmptyException;
import ru.t1.avfilippov.tm.marker.UnitCategory;
import ru.t1.avfilippov.tm.model.User;
import ru.t1.avfilippov.tm.repository.ProjectRepository;
import ru.t1.avfilippov.tm.repository.TaskRepository;
import ru.t1.avfilippov.tm.repository.UserRepository;

import java.util.Objects;

import static ru.t1.avfilippov.tm.constant.TestData.*;

@Category(UnitCategory.class)
public final class UserServiceTest {

    @NotNull
    private final IUserRepository repository = new UserRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final UserService service = new UserService(repository,propertyService,projectRepository,taskRepository);

    @NotNull
    private static final String LOGIN_TEST = "logintest";

    @NotNull
    private static final String PASS_TEST = "logintest";

    @NotNull
    private static final String PASS_RETEST = "logintest";

    @NotNull
    private static final String NAME = "firstName";

    @NotNull
    private User userTesting;

    @Before
    public void before() throws AbstractException {
        userTesting = service.create(LOGIN_TEST,PASS_TEST, Role.USUAL);
    }

    @After
    public void after() {
        service.clear();
    }

    @Test
    public void create() throws AbstractException {
        service.create(LOGIN,PASSWORD, Role.USUAL);
        @Nullable final User user = service.findByLogin(LOGIN);
        Assert.assertNotNull(user);
        projectRepository.add(user.getId(),USER_PROJECT1);
        Assert.assertNotNull(projectRepository.findOneById(USER_PROJECT1.getId()));
    }

    @Test
    public void findByLogin() throws LoginEmptyException, UserNotFoundException {
        Assert.assertNotNull(service.findByLogin(LOGIN_TEST));
    }


    @Test
    public void removeByLogin() throws AbstractFieldException {
        service.removeByLogin(LOGIN_TEST);
        Assert.assertThrows(Exception.class, () -> service.findByLogin(LOGIN_TEST));
    }

    @Test
    public void updateUser() throws AbstractFieldException {
        service.updateUser(userTesting.getId(),NAME,"lastName","middle");
        Assert.assertTrue(Objects.equals(userTesting.getFirstName(), NAME));
    }

    @Test
    public void isLoginExist() {
        Assert.assertTrue(service.isLoginExist(LOGIN_TEST));
    }

}
