package ru.t1.avfilippov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.avfilippov.tm.api.repository.ITaskRepository;
import ru.t1.avfilippov.tm.marker.DataCategory;
import ru.t1.avfilippov.tm.model.Task;

import static ru.t1.avfilippov.tm.constant.TestData.*;

@Category(DataCategory.class)
public final class TaskRepositoryTest {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Before
    public void before() {
        taskRepository.add(USER_TASK1);
        taskRepository.add(ADMIN_TASK1);
    }

    @After
    public void after() {
        taskRepository.add(USER_TASK2);
        taskRepository.clear();
    }

    @Test
    public void add() {
        Assert.assertNotNull(taskRepository.add(USER_TASK2));
        Assert.assertNull(taskRepository.add(NULL_TASK));
        @Nullable final Task task = taskRepository.findOneById(USER_TASK2.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK2, task);
    }

    @Test
    public void addByUserId() {
        Assert.assertNotNull(taskRepository.add(USER1.getId(), USER_TASK2));
    }

    @Test
    public void createByUserId() {
        Assert.assertEquals(ADMIN_TASK1.getUserId(), USER2.getId());
    }

    @Test
    public void findAllIfOne() {
        @NotNull final TaskRepository emptyRepository = new TaskRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_TASK1);
        Assert.assertEquals(USER_TASK1, emptyRepository.findOneById(USER_TASK1.getId()));
    }

    @Test
    public void findById() {
        Assert.assertNotNull(taskRepository.findOneById(USER_TASK1.getId()));
    }

    @Test
    public void removeById() {
        Assert.assertNotNull(taskRepository.removeById(ADMIN_TASK1.getId()));
    }

    @Test
    public void removeByIndex() {
        Assert.assertNotNull(taskRepository.removeByIndex(1));
    }

    @Test
    public void removeAll() {
        taskRepository.clear();
        Assert.assertTrue(taskRepository.findAll().isEmpty());
    }

    @Test
    public void create() {
        @NotNull final Task task = taskRepository.create(USER2.getId(), ADMIN_TASK1.getName(), ADMIN_TASK1.getDescription());
        Assert.assertEquals(ADMIN_TASK1.getDescription(), task.getDescription());
        Assert.assertEquals(ADMIN_TASK1.getName(), task.getName());
        Assert.assertEquals(USER2.getId(), task.getUserId());
    }

}
