package ru.t1.avfilippov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.avfilippov.tm.api.repository.ISessionRepository;
import ru.t1.avfilippov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.avfilippov.tm.exception.field.UserIdEmptyException;
import ru.t1.avfilippov.tm.marker.DataCategory;

import static ru.t1.avfilippov.tm.constant.TestData.SESSION;
import static ru.t1.avfilippov.tm.constant.TestData.USER1;

@Category(DataCategory.class)
public final class SessionRepositoryTest {

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @Before
    public void before() throws UserIdEmptyException, ProjectNotFoundException {
        sessionRepository.add(USER1.getId(), SESSION);
    }

    @After
    public void after() throws UserIdEmptyException {
        sessionRepository.clear(USER1.getId());
    }

    @Test
    public void add() throws UserIdEmptyException, ProjectNotFoundException {
        Assert.assertNotNull(sessionRepository.add(SESSION));
    }

    @Test
    public void addByUserId() throws UserIdEmptyException, ProjectNotFoundException {
        Assert.assertNotNull(sessionRepository.add(USER1.getId(), SESSION));
    }

    @Test
    public void createByUserId() {
        Assert.assertEquals(SESSION.getUserId(), USER1.getId());
    }

    @Test
    public void findAllNull() {
        @NotNull final SessionRepository sessionRepository = new SessionRepository();
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
    }

    @Test
    public void findByNullId() {
        Assert.assertNull(sessionRepository.findOneById(USER1.getId(), null));
    }

}
